<?php

namespace app;

use yii;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getViewPath() {
        return '@app/views';
    }

    /**
     * Стартовая страница
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Форма обратной связи
     *
     * @return string|yii\web\Response
     */
    public function actionFeedback() {

        $model = new FeedbackRecord();
        if ($model->load(Yii::$app->request->post())) {
            yii::$app->session->setFlash('FeedbackSubmitted');
            $model->save();

            return $this->refresh();
        }
        return $this->render('feedback', [
            'model' => $model,
        ]);
    }

    /**
     * Проверка поиска по текстовому файлу
     *
     * @return string
     */
    public function actionSearch() {
        if(!SeekableModel::hasFile()) {
            return $this->render('info', [
                'message' => 'Нет файла для поиска',
                'link' => 'generate-file',
                'text' => 'Сгенерировть файл',
            ]);
        }

        if(!SeekableModel::hasIndex()) {
            return $this->render('info', [
                'message' => 'Отсутствует файл индекса',
                'link' => 'generate-file',
                'text' => 'Построить индекс',
            ]);
        }

        $seeker = new SeekableModel();

        for($i=0; $i < 10; $i++) {
            $keys[] = rand(0, $seeker->getMaxPos());
        }
        return $this->render('search', ['seeker' => $seeker, 'keys' => $keys]);
    }

    public function actionSearch10000() {
        $seeker = new SeekableModel();
        $max = $seeker->getMaxPos();
        $start = microtime(true);
        for($i=0; $i < 10000; $i++) {
            $n = rand(0, $max);
            $seeker->getLine($n);
        }
        $end = microtime(true);
        $time = $end - $start;
        return $this->render('info', [
            'message' => "$time seconds for 10 000 reads",
            'link' => 'search',
            'text' => 'Поиск'
        ]);
    }

    /**
     * Построение индекса для поиска
     *
     * @return string
     */
    public function actionCreateIndex() {
        SeekableModel::createIndex();
        return $this->render('info', [
            'message' => 'Индекс готов',
            'link' => 'search',
            'text' => 'Протестировать поиск',
        ]);
    }

    /**
     * Генерация тестового файла для поиска
     *
     * @return string
     */
    public function actionGenerateFile() {
        SeekableModel::generateFile();
        return $this->render('info', [
            'message' => 'Файл для поиска сгенерирован',
            'link' => 'create-index',
            'text' => 'Построить индекс',
        ]);
    }

    /**
     * Страница сортировки таблицы
     *
     * @return string
     */
    public function actionTable() {
        $this->layout = false;
        return $this->render('01');
    }
}