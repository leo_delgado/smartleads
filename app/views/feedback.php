<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeedbackModel */
/* @var $form ActiveForm */
?>

<div class="feedback">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Contact us</h4>
                </div>

                    <?php if (Yii::$app->session->hasFlash('FeedbackSubmitted')): ?>

                        <div class="modal-body">

                        <p>Thank you for your feedback!</p>

                        </div>

                     <?php else: ?>

                <?php $form = ActiveForm::begin(); ?>

                        <div class="modal-body">

                            <?= $form->field($model, 'name') ?>
                            <?= $form->field($model, 'email') ?>
                            <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

                        </div>
                        <div class="modal-footer">
                            <div class="form-group">
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>

                <?php endif; ?>
              </div>
            </div>
        </div>

    </div>


</div><!-- feedback -->
