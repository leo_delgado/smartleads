<?php

namespace app;

use yii;

/**
 * SeekableModel Класс, реализуюций алгоритм поиска в большом файле ~2Gb
 *
 * для проверки алгоритма константы класса настроена на небольшой размер файла -- 1M
 * готовый тестовый файл с пронумерованными строками можно скопировать в каталог runtime,
 * либо веб интерфейс тестовых задач предложить сгенерировать новый файл.
 *
 * При желании можно раскомментировать константы для большого файла и
 * поэкспериментировать со своим тестовым файлом
 *
 * @package app
 */
class SeekableModel implements \SeekableIterator
{
    const FILE_NAME = 'test.txt'; // в тестовой задаче используем фиксированное имя фала
    const LINE_LIMIT = 80;


//    const FILE_SIZE = 1 << 31; // 2Gb
//    const CHUNK_SIZE = 1 << 22; // 4M

    const FILE_SIZE = 1 << 20; // 1M
    const CHUNK_SIZE = 1 << 16; // 64K

    private $_file = null;
    private $_chunks = null;
    private $_path = null;
    private $_maxPos = null;
    private $_position = 0;

    /**
     * {@inheritdoc}
     */
    public function seek($position)
    {
        if ($position > $this->_maxPos) {
            throw new OutOfBoundsException("Position is out of range");
        }
        $this->_position = $position;
    }
    /**
     * {@inheritdoc}
     */
    public function rewind() {
        $this->_position = 0;
    }
    /**
     * {@inheritdoc}
     */
    public function current() {
        return $this->getLine($this->_position);
    }
    /**
     * {@inheritdoc}
     */
    public function key() {
        return $this->_position;
    }
    /**
     *
     */
    public function next() {
        ++$this->_position;
    }
    /**
     * {@inheritdoc}
     */
    public function valid() {
        return $this->_position <= $this->_maxPos;
    }


    /**
     * Во время инициализации открываем тестовый файл на чтение
     * и загружаем список блоков индекса, попутно вычисляем
     * максимальное значение ключа поиска
     */
    public function __construct() {

        $this->_path = yii::getAlias('@runtime/');
        $this->_file = fopen($this->_path . self::FILE_NAME, 'r');
        $this->_chunks = explode("\n", file_get_contents($this->_path . "chunks.txt"));

        $count = 0;
        foreach($this->_chunks as $n) {
            $count += $n;
        }
        $this->_maxPos = $count - 1;
    }

    /**
     * закрываем дескриптов тестового файла
     */
    public function __destruct() {
        fclose($this->_file);
    }

    /**
     * Максимальная позиция
     * @return int
     */
    public function getMaxPos() {
        return $this->_maxPos;
    }

    /**
     * Путь к блоку индекса по номеру блока
     * @param $n
     * @return string
     */
    private function _getIndexFileName($n) {
        return $this->_path . sprintf('index%03d.txt', $n);
    }

    /**
     * Построение интекса
     */
    public static function createIndex() {

        $filename = yii::getAlias('@runtime/') . self::FILE_NAME;
        $file = fopen($filename, 'r');
        $pos = 0;
        $chunk = 0;
        $path = yii::getAlias('@runtime/');
        $chunksInfo = [];

        while ($content = fread($file, self::CHUNK_SIZE)) {
            $index = [];
            $lines = explode("\n",$content);
            $n = count($lines) - 1; // последнюю строку отбрасываем, она оборванная
            for($i=0; $i < $n; $i++) {
                $index[$i] = $pos;
                $pos += strlen($lines[$i]) + 1; // +1 для символа перевода строки
            }
            file_put_contents( $path . sprintf('index%03d.txt', $chunk), implode("\n", $index) );
            $chunksInfo[$chunk] = $n;
            $chunk++;
            fseek($file, $pos);
        }

        file_put_contents($path . 'chunks.txt', implode("\n", $chunksInfo));
    }

    /**
     * Генерация тестового файла
     */
    public static function generateFile() {

        $words = file(yii::getAlias('@migrations/words.txt'));
        $file = fopen(yii::getAlias('@runtime/') . self::FILE_NAME, 'w');

        $lineNum = 0;
        $totalLength = 0;
        while($totalLength < self::FILE_SIZE) {
            $line = sprintf("%05d ", $lineNum++);
            while (strlen($line) < self::LINE_LIMIT) {
                $line .= ' ' . trim($words[array_rand($words)]);
            }
            $line .= "\n";
            $totalLength += strlen($line);
            fwrite($file, $line);
        }

        fclose($file);
    }

    /**
     * Поиск строки по номеру позиции
     * @param $n
     * @return bool|string
     */
    public function getLine($n) {
        $this->seek($n);
        $i = 0;
        $linesAhead = $n;
        while($linesAhead >= $this->_chunks[$i]) {
            $linesAhead -= $this->_chunks[$i++];
        }

        $index = explode("\n", file_get_contents($this->_getIndexFileName($i)));

        $offset = $index[$linesAhead];
        fseek($this->_file, $offset);
        $line = fgets($this->_file);
        return $line;
    }

    /**
     * Проверка наличия тестового файла
     * @return bool
     */
    public static function hasFile() {
        $filename = yii::getAlias('@runtime/') . self::FILE_NAME;
        return is_readable($filename);
    }

    /**
     * Проверка наличия индекса
     * @return bool
     */
    public static function hasIndex() {
        $filename = yii::getAlias('@runtime/') . 'chunks.txt';
        return is_readable($filename);
    }
}