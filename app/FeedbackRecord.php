<?php

namespace app;

use yii;

/**
 * модель Active record для таблицы "feedback".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $message
 */
class FeedbackRecord extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message', 'name', 'email'], 'required'],
            [['message'], 'string'],
            [['name', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'message' => 'Message',
        ];
    }
}
