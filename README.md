** Установка проекта **

1. Клонируйте репозиторий
2. Запустите composer install
3. Убедитесь, что каталоги runtime и web/assets открыты для записи веб-серверу
4. Настройте доступ к базе MySql, см. config.php, секция db. При необходимости приведите в  соответствие параметры username, password, dbname
5. Создайте в базе данных таблицу feedback, см. migrations/feedback.sql
6. Необязательно: скопируйте migratinos/test.txt в runtime