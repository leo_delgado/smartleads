<?php
return [
    'id' => 'smart-leads-test-app',
    'basePath' => __DIR__,
    'controllerNamespace' => 'app',
    'aliases' => [
        '@app' => __DIR__ . '/app',
        '@migrations' => __DIR__ . '/migrations',
        '@runtime' => __DIR__ . '/runtime',
        '@bower' => '@vendor/bower-asset',
    ],
    'layoutPath' => '@app/views',
    'layout' => 'layout.php',
    'components' => [
        'request' => [
            'cookieValidationKey' => '642cd28ad583dc1749d62359067baeb2',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=test',
            'username' => 'test',
            'password' => 'secret',
            'charset' => 'utf8',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<action:[\w-]+>' => 'site/<action>',
            ],
        ],
    ],

];